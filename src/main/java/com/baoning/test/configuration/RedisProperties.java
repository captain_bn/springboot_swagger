package com.baoning.test.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created By BaoNing On 2018/12/5
 */
@Data
@Component
@ConfigurationProperties("redis")
public class RedisProperties {

    public String host;

    public Integer port;

    public String password;

}

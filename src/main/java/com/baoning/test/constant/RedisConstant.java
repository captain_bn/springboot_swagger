package com.baoning.test.constant;

/**
 * Created By BaoNing On 2018/12/5
 */

public class RedisConstant {

    public static final String Redis_CONNECTION_FACTORY = "redisConnectionFactory";

    public static final String REDIS_TEMPLATE = "redisTemplate";


    /**
     * 设置过期时间
     */
    public static final class EXPIRE {

        public static final int TIME = 5 * 60 * 60; // 5 hours

    }


}

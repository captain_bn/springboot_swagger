package com.baoning.test.service;

import com.baoning.test.domain.User;
import com.baoning.test.vo.ArrayVo;
import com.baoning.test.vo.SearchVo;
import com.github.pagehelper.Page;

/**
 * Created By BaoNing On 2018/12/4
 */
public interface UserService {

    Page<User> getUserList(Integer pageNum, Integer pageSize);

    User getUserById(Integer userId);

    User getUserByName(SearchVo searchVo);

    int insertUser(User user);

    boolean updateUser(User user);

    boolean deleteUserById(Integer userId);

    boolean deleteUsersByIds(ArrayVo arrayVo);


}

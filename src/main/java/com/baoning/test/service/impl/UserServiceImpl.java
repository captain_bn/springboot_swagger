package com.baoning.test.service.impl;

import com.baoning.test.constant.RedisConstant;
import com.baoning.test.domain.User;
import com.baoning.test.mapper.UserMapper;
import com.baoning.test.mapper.UsersMapper;
import com.baoning.test.service.UserService;
import com.baoning.test.vo.ArrayVo;
import com.baoning.test.vo.SearchVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created By BaoNing On 2018/12/4
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UsersMapper usersMapper;

    @Autowired
    private RedisTemplate<String,User> redisTemplate;

    /**
     * 分页展示用户信息
     * @return
     */
    @Override
    public Page<User> getUserList(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return userMapper.getUserList();
    }

    /**
     * 通过userId获取用户
     * @param userId
     * @return
     */
    @Override
    public User getUserById(Integer userId) {
        return userMapper.getUserById(userId);
    }

    /**
     * 通过用户名称查询
     * @param searchVo
     * @return
     */
    @Override
    public User getUserByName(SearchVo searchVo) {
        String userName = searchVo.getUserName();
        //读取缓存数据
        User user = redisTemplate.opsForValue().get(userName);
        if (user == null){
            return userMapper.getUserByName(userName);
        }
        return user;
    }

    /**
     * 新增用户
     * @param user
     * @return
     */
    @Override
    public int insertUser(User user) {
        String name = user.getName();
        redisTemplate.opsForValue().set(name,user, RedisConstant.EXPIRE.TIME, TimeUnit.SECONDS);
        return userMapper.insertUser(user);
    }

    /**
     * 修改用户
     * @param userVo
     * @return
     */
    @Override
    public boolean updateUser(User userVo) {
        User user = userMapper.getUserById(userVo.getId());
        if(user != null){
            return userMapper.updateUser(userVo);
        }
        return false;
    }

    /**
     * 根据userId删除用户
     * @param userId
     * @return
     */
    @Override
    public boolean deleteUserById(Integer userId) {
        return userMapper.deleteUserById(userId);
    }

    /**
     * 批量删除用户
     * @param arrayVo
     * @return
     */
    @Override
    public boolean deleteUsersByIds(ArrayVo arrayVo) {
        List<Integer> ids = arrayVo.getUserIds();
        return usersMapper.deleteUsersByIds(ids);
    }


}

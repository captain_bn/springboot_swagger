package com.baoning.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created By BaoNing On 2018/12/5
 */
@Configuration
@EnableSwagger2  //开启在线接口文档
public class SwaggerConfig {

    /**
     * 添加摘要信息(Docket)
     */
    @Bean
    public Docket controllerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("用户模块接口文档")
                        .description("用户管理模块")
                        .contact(new Contact("BaoNing", null, null))
                        .version("v-1.0.0")
                        .build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.baoning.test.controller"))
                .paths(PathSelectors.any())
                .build();
    }

}

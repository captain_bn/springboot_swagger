package com.baoning.test.config;

import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
import com.baoning.test.configuration.RedisProperties;
import com.baoning.test.constant.RedisConstant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;


/**
 * Created By BaoNing On 2018/12/5
 */
@Configuration
public class RedisConfig {

    @Autowired
    RedisProperties redisProperties;


    @Bean(RedisConstant.REDIS_TEMPLATE)
    RedisTemplate cacheRedisTemplate(@Qualifier(RedisConstant.Redis_CONNECTION_FACTORY) RedisConnectionFactory cacheConnectionFactory) {
        return redisTemplate(cacheConnectionFactory);
    }


    @Bean(RedisConstant.REDIS_TEMPLATE)
    RedisTemplate defaultRedisTemplate(@Qualifier(RedisConstant.Redis_CONNECTION_FACTORY) RedisConnectionFactory cacheConnectionFactory) {
        return cacheRedisTemplate(cacheConnectionFactory);
    }


    private RedisConnectionFactory connectionFactory(int database) {
        RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
        configuration.setHostName(redisProperties.host);
        configuration.setPort(redisProperties.port);
        if(!StringUtils.isEmpty(redisProperties.password)) {
            configuration.setPassword(RedisPassword.of(redisProperties.password));
        }
        configuration.setDatabase(database);
        return new LettuceConnectionFactory(configuration);
    }

    private RedisTemplate redisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(connectionFactory);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        GenericFastJsonRedisSerializer fastJsonRedisSerializer = new GenericFastJsonRedisSerializer();
        redisTemplate.setValueSerializer(fastJsonRedisSerializer);
        return redisTemplate;
    }



}

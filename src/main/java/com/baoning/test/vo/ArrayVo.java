package com.baoning.test.vo;

import lombok.Data;

import java.util.List;

/**
 * Created By BaoNing On 2018/12/4
 */
@Data
public class ArrayVo {

    /**
     * 用户id集合
     */
    public List<Integer> userIds;

}

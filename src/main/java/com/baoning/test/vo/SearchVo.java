package com.baoning.test.vo;

import lombok.Data;

/**
 * Created By BaoNing On 2018/12/6
 */
@Data
public class SearchVo {

    public String userName;

}

package com.baoning.test.vo;

import com.baoning.test.domain.User;
import lombok.Data;

/**
 * Created By BaoNing On 2018/12/4
 */
@Data
public class UserVo extends User{

}

package com.baoning.test.mapper;

import com.baoning.test.domain.User;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.*;

/**
 * Created By BaoNing On 2018/12/4
 */
@Mapper
public interface UserMapper {

    /**
     * 分页展示用户信息
     * @return
     */
    @Select(" select * from user ")
    Page<User> getUserList();

    /**
     * 通过userId获取用户
     * @param userId
     * @return
     */
    @Select(" select * from user where id = #{userId} ")
    User getUserById(@Param("userId") Integer userId);

    /**
     * 根据用户名称查询
     * @param name
     * @return
     */
    @Select(" select * from user where name = #{name} ")
    User getUserByName(@Param("name") String name);

    /**
     * 新增用户
     * @param user
     * @return
     */
    @Insert(" insert into user (id, name, phone, identity_card, location, sex, qq, weixin, mail ) values " +
            "(#{id}, #{name}, #{phone}, #{identityCard}, #{location}, #{sex}, #{qq}, #{weixin}, #{mail} ) ")
    int insertUser(User user);

    /**
     * 修改用户
     * @param user
     * @return
     */
    @Update(" update user set name=#{name},phone=#{phone} where id=#{id} ")
    boolean updateUser(User user);

    /**
     * 根据userId删除用户
     * @param userId
     * @return
     */
    @Delete(" delete from user where id =#{userId} ")
    boolean deleteUserById(@Param("userId") Integer userId);



}

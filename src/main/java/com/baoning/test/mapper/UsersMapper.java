package com.baoning.test.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created By BaoNing On 2018/12/4
 */
@Mapper
public interface UsersMapper {

    /**
     * 根据userIds批量删除用户
     * @param userIds
     * @return
     */
    boolean deleteUsersByIds(@Param("userIds") List<Integer> userIds);


}

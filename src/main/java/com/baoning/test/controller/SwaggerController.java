package com.baoning.test.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Created By BaoNing On 2018/12/4
 */
@ApiIgnore
@Controller
public class SwaggerController {

    /**
     * api文档
     * @return String
     */
    @RequestMapping(value = "/api-docs")
    public String index() {
        return "redirect:swagger-ui.html";
    }

}

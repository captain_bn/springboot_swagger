package com.baoning.test.controller;

import com.baoning.test.domain.User;
import com.baoning.test.service.UserService;
import com.baoning.test.vo.ArrayVo;
import com.baoning.test.vo.SearchVo;
import com.baoning.test.vo.UserVo;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created By BaoNing On 2018/12/4
 */
@Api(tags = "用户管理模块")
@RestController
@RequestMapping("api/v1/user")
public class UserController {

    @Autowired
    private UserService userService;


    /**
     * 分页展示用户信息
     * @param pageNum
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "分页展示", notes = "分页展示用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum",value = "pageNum",required = true, dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "pageSize",value = "pageSize",required = true, dataType = "Integer", paramType = "query")
    })
    @GetMapping("/list")
    public Page<User> getUserList(@RequestParam("pageNum")Integer pageNum,
                                  @RequestParam("pageSize") Integer pageSize){
        return userService.getUserList(pageNum,pageSize);
    }

    /**
     * 通过userId获取用户信息
     * @param userId
     * @return
     */
    @ApiOperation(value = "根据id查询", notes = "通过id查询用户")
    @ApiImplicitParam(name = "userId",value = "用户id",required = true,dataType = "Integer", paramType = "path")
    @GetMapping("/id/{userId}")
    public User getUserById(@PathVariable("userId") Integer userId){
        return userService.getUserById(userId);
    }

    /**
     * 通过name获取用户信息
     * @param searchVo
     * @return
     */
    @ApiOperation(value = "根据name查询", notes = "通过name查询用户")
    @ApiImplicitParam(name = "searchVo",value = "用户名称",required = true,dataType = "SearchVo", paramType = "body")
    @PostMapping("/search")
    public User getUserByName(@RequestBody SearchVo searchVo){
        return userService.getUserByName(searchVo);
    }

    /**
     * 新增用户
     * @param userVo
     * @return
     */
    @ApiOperation(value = "新增用户", notes = "增加用户")
    @PostMapping("/insert")
    public int insertUser(@RequestBody UserVo userVo){
        return userService.insertUser(userVo);
    }

    /**
     * 修改用户
     * @param userVo
     * @return
     */
    @ApiOperation(value = "修改用户", notes = "修改用户信息")
    @PostMapping("/update")
    public boolean updateUser(@RequestBody UserVo userVo){
        return userService.updateUser(userVo);
    }

    /**
     * 删除用户
     * @param userId
     * @return
     */
    @ApiOperation(value = "删除用户", notes = "通过id删除用户")
    @ApiImplicitParam(name = "userId",value = "用户id",required = true,dataType = "Integer", paramType = "path")
    @DeleteMapping("/delete/{userId}")
    public boolean deleteUserById(@PathVariable("userId") Integer userId){
        return userService.deleteUserById(userId);
    }

    /**
     * 批量删除用户
     * @param arrayVo
     * @return
     */
    @ApiOperation(value = "批量删除用户", notes = "通过ids批量删除用户")
    @PostMapping("/deleteIds")
    public boolean deleteByUserIds(@RequestBody ArrayVo arrayVo){
        return userService.deleteUsersByIds(arrayVo);
    }



}

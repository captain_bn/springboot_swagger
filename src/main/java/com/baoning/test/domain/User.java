package com.baoning.test.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created By BaoNing On 2018/12/4
 */
@Data
public class User {

    @ApiModelProperty(value="用户id")
    private Integer id;

    @ApiModelProperty(value="用户名")
    private String name;

    @ApiModelProperty(value="手机号")
    private String phone;

    @ApiModelProperty(value="身份证号")
    private String identityCard;

    @ApiModelProperty(value="地址")
    private String location;

    @ApiModelProperty(value="性别")
    private String sex;

    @ApiModelProperty(value="qq")
    private String qq;

    @ApiModelProperty(value="微信")
    private String weixin;

    @ApiModelProperty(value="邮箱")
    private String mail;

}
